class Bullet {
	constructor() {
		this.id = ++game.bulletsNumber; // zwiekszenie liczby wystrzelonych pociskow od poczatku istnienia gry oraz przypisanie na jej podstawie id pocisku
		this.sprite = new Image();
		this.sprite.src = "./gfx/spritesheet.png";

		/* położenie pocisku na dziobie statku */
		this.x = game.ship.upperPointX; // punkt poczatkowy pocisku na dziobie statku, x
		this.y = game.ship.upperPointY; // punkt poczatkowy pocisku na dziobie statku, y

		this.width = 6;
		this.height = 22;

		this.bulletAngle = game.ship.shipAngle; // kierunek w jakim leci przycisk na podstawie aktualnego kata/polozenia statku
		this.speed = 10; // predkosc pocisku
	}

	draw() {

		// jesli pociski nie sa widoczne w oknie przegladarki to usun je z tablicy pociskow 
		if (this.x < 0 || this.x > window.innerWidth ||
			this.y < 0 || this.y > window.innerHeight
			) {
			delete game.bulletsArray[this.id];
		}

		this.x -=Math.sin(Math.PI / 180 * this.bulletAngle) * this.speed; // polozenie pocisku x
		this.y -=Math.cos(Math.PI / 180 * this.bulletAngle) * this.speed; // polozenie pocisku y
		
		// rysowanie sprite'a
		game.ctx.save();
		game.ctx.translate(this.x, this.y);
		game.ctx.rotate(-this.bulletAngle * Math.PI / 180);
		game.ctx.translate(-this.x, -this.y);
		game.ctx.drawImage(this.sprite, 843, 137, 6, 22, this.x - this.width / 2, this.y - this.height / 2, this.width, this.height);
		game.ctx.restore();		
	}
}
