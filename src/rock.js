class Rock {
	constructor() {
		this.id = ++game.rocksNumber; // zwiekszenie liczby kamieni od poczatku istnienia gry oraz przypisanie na jej podstawie id kamienia
		this.sprite = new Image();
		this.sprite.src = "./gfx/rock.png";

		this.r = this.rand(15, 40) * 0.002 * Math.min(window.innerWidth, window.innerHeight); // losowy promien kamienia
		this.speed = 0.00007 * this.rand(1, 5); // predkosc kamienia

		this.x = ((this.rand(0,1) ? this.rand(0,30) : this.rand(70,100))) / 100 * window.innerWidth; // losujemy polozenie kamienia x
		this.y = ((this.rand(0,1) ? this.rand(0,30) : this.rand(70,100))) / 100 * window.innerHeight; // losujemy polozenie kamienia y

		this.modX = ((this.rand(0,1) ? this.rand(-10, -1) : this.rand(1, 10))) * this.speed; // zmiana modX kamienia
		this.modY = ((this.rand(0,1) ? this.rand(-10, -1) : this.rand(1, 10))) * this.speed; // zmiana modY kamienia

		this.rockRotation = 0;
		this.changeRotation = this.rand(-3, 3);
	}

	bulletCollision() {
		for (var property in game.bulletsArray) {
			if (game.bulletsArray[property].x >= this.x - this.r && game.bulletsArray[property].x <= this.x + this.r &&
				game.bulletsArray[property].y >= this.y - this.r && game.bulletsArray[property].y <= this.y + this.r
				) {
				game.boomSound.play();
				game.score +=  Math.round(150 / this.r); // zliczanie punktow

				for (var i = 0; i < this.rand(2, 4); i++) {
					game.smallRock = new SmallRock(this.x, this.y); // stworzenie malego kamienia
					game.smallRocksArray[game.smallRock.id] = game.smallRock; // dodawanie malego kamienia do pseudo tablicy
				}
				delete game.rocksArray[this.id]; // usuwanie konkretnego kamienia w przypadku kolizji
				delete game.bulletsArray[property]; // usuwanie konkretnego pocisku w przypadku kolizji
				game.rocksNumber--;

				//this.points = Math.round(150 / this.r);
				// game.ctx.fillStyle = 'red';
				//game.ctx.fillText(this.points + ' pts', this.x, this.y); // wyswietlanie punktow w miejscu zniszczonego kamienia
				// game.ctx.fillStyle = 'white';
			}				
		}
	}

	draw() {
		this.x += this.modX * window.innerWidth; // zmiana polozenia kamienia x
		this.y += this.modY * window.innerHeight; // zmiana polozenia kamienia y

		//wyjscie kamienia poza granice canvas
		if (this.x + this.r < 0) {
			this.x += window.innerWidth + (2 * this.r);
		} else if (this.x - this.r > window.innerWidth) {
			this.x -= window.innerWidth + (2 * this.r);
		}

		if (this.y + this.r < 0) {
			this.y += window.innerHeight + (2 * this.r);
		} else if (this.y - this.r > window.innerHeight) {
			this.y -= window.innerHeight + (2 * this.r);
		}

		this.rockRotation += this.changeRotation; //obrot kamienia w losowym kierunku i kacie
		// rysowanie sprite'a
		game.ctx.save();
		game.ctx.translate(this.x, this.y);
		game.ctx.rotate(this.rockRotation * Math.PI / 180);
		game.ctx.translate(-this.x, -this.y);
		game.ctx.drawImage(this.sprite, this.x - this.r, this.y - this.r, 2 * this.r, 2 * this.r);
		game.ctx.restore();
	}

	rand(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
}