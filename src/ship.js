class Ship {
	constructor() {
		this.sprite = new Image();
		this.sprite.src = "./gfx/spritesheet2.png";
		// this.sprite.src = "./gfx/shit.png";

		this.r = 0.03 * Math.min(window.innerWidth, window.innerHeight); // obliczamy r wzgledem aktualnie najmniejszego wymiaru okna
		this.shipAngle = 0; // zmiana kata statku
		this.bowAngle = 180; // kat dziobu statku
		this.sternAngle = 45; // katy z tylu statku

		this.x = window.innerWidth / 2; // poczatkowe wspolrzedne statku
		this.y = window.innerHeight / 2; // poczatkowe wspolrzedne statku

		this.height = 0.055 * Math.min(window.innerWidth, window.innerHeight);
		this.width = 0.055 * Math.min(window.innerWidth, window.innerHeight);

		this.modX = 0; // przyrost x, wartosc poczatkowa = 0
		this.modY = 0; // przyrost y, wartosc poczatkowa = 0

		this.accel = 0.0001 * Math.min(window.innerWidth, window.innerHeight); // modyfikator predkosci, bez tego statek strasznie szybko leci
		
		this.maxMod = 4; // maksymalna zmiana modX i modY
	}

	rocksCollision() {
		for (var property in game.rocksArray) {
			// sprawdzamy czy ktorys z 3 punktow statku zetknal sie z ktoryms z kamieni
			if (this.upperPointX >= game.rocksArray[property].x - game.rocksArray[property].r && this.upperPointX <= game.rocksArray[property].x + game.rocksArray[property].r &&
				this.upperPointY >= game.rocksArray[property].y - game.rocksArray[property].r && this.upperPointY <= game.rocksArray[property].y + game.rocksArray[property].r ||

				this.leftPointX >= game.rocksArray[property].x - game.rocksArray[property].r && this.leftPointX <= game.rocksArray[property].x + game.rocksArray[property].r &&
				this.leftPointY >= game.rocksArray[property].y - game.rocksArray[property].r && this.leftPointY <= game.rocksArray[property].y + game.rocksArray[property].r ||

				this.rightPointX >= game.rocksArray[property].x - game.rocksArray[property].r && this.rightPointX <= game.rocksArray[property].x + game.rocksArray[property].r &&
				this.rightPointY >= game.rocksArray[property].y - game.rocksArray[property].r && this.rightPointY <= game.rocksArray[property].y + game.rocksArray[property].r
				) {
				game.boomSound.play();

				for (var i = 0; i < this.rand(2, 4); i++) {
					game.smallRock = new SmallRock(game.rocksArray[property].x, game.rocksArray[property].y); // stworzenie malego kamienia
					game.smallRocksArray[game.smallRock.id] = game.smallRock; // dodawanie malego kamienia do pseudo tablicy
				}
				delete game.ship;	// usuwanie obecnego statku
				delete game.rocksArray[property]; // usuwanie konkretnego kamienia w przypadku kolizji
				--game.lives; // przy zderzeniu zmniejszamy zycie o 1

				if (game.lives > 0) {
					game.ship = new Ship(); // utworzenie nowego statku
				} else {
					game.over(); // aby grac dalej wykup premium u wujka artura
				}
			}
		}
		
		for (var property in game.smallRocksArray) {
			// sprawdzamy czy ktorys z 3 punktow statku zetknal sie z ktoryms z kamieni
			if (this.upperPointX >= game.smallRocksArray[property].x - game.smallRocksArray[property].r && this.upperPointX <= game.smallRocksArray[property].x + game.smallRocksArray[property].r &&
				this.upperPointY >= game.smallRocksArray[property].y - game.smallRocksArray[property].r && this.upperPointY <= game.smallRocksArray[property].y + game.smallRocksArray[property].r ||

				this.leftPointX >= game.smallRocksArray[property].x - game.smallRocksArray[property].r && this.leftPointX <= game.smallRocksArray[property].x + game.smallRocksArray[property].r &&
				this.leftPointY >= game.smallRocksArray[property].y - game.smallRocksArray[property].r && this.leftPointY <= game.smallRocksArray[property].y + game.smallRocksArray[property].r ||

				this.rightPointX >= game.smallRocksArray[property].x - game.smallRocksArray[property].r && this.rightPointX <= game.smallRocksArray[property].x + game.smallRocksArray[property].r &&
				this.rightPointY >= game.smallRocksArray[property].y - game.smallRocksArray[property].r && this.rightPointY <= game.smallRocksArray[property].y + game.smallRocksArray[property].r
				) {
				game.boomSound.play();
				delete game.ship;	// usuwanie obecnego statku
				delete game.smallRocksArray[property]; // usuwanie konkretnego kamienia w przypadku kolizji
				--game.lives; // przy zderzeniu zmniejszamy zycie o 1

				if (game.lives > 0) {
					game.ship = new Ship(); // utworzenie nowego statku
				} else {
					game.over(); // aby grac dalej wykup premium u wujka artura
				}
			}
		}
	}

	draw() {		
		if (game.keysArray['key_37'] === true) {
			this.shipAngle += 5; // obrot statku w lewo
			// this.current = 0;
		}

		if (game.keysArray['key_39'] === true) {
			this.shipAngle -= 5; // obrot statku w prawo
			// this.current = 2;
		}

		if (game.keysArray['key_38'] === true) {
			this.modX = Math.max(-this.maxMod, Math.min(this.maxMod, this.modX - Math.sin(Math.PI / 180 * this.shipAngle) * this.accel)); // modyfikacja polozenia statku x
			this.modY = Math.max(-this.maxMod, Math.min(this.maxMod, this.modY - Math.cos(Math.PI / 180 * this.shipAngle) * this.accel)); // modyfikacja polozenia statku y
		} else {
			this.modX *= 0.99; // statek powoli spowalnia x
			this.modY *= 0.99; // statek powoli spowalnia y
		}

		if (game.keysArray['key_40'] === true) {
			this.modX *= 0.965; // hamowanie statkiem x
			this.modY *= 0.965; // hamowanie statkiem y
		}

		this.x += this.modX; // zmiana wspolrzednych srodka statku wpisanego w okrag o wartosc modX
		this.y += this.modY; // zmiana wspolrzednych srodka statku wpisanego w okrag o wartosc modY

		this.leftPointX = this.x + Math.sin(Math.PI / 180 * (this.shipAngle - this.sternAngle)) * 0.6 * this.r; // sinA = x/r => x = sinA * r // * 0.6 dla zwezenia bokow statku
		this.leftPointY = this.y + Math.cos(Math.PI / 180 * (this.shipAngle - this.sternAngle)) * 0.6 * this.r; // cosA = y/r => y = cosA * r // * 0.6 dla zwezenia bokow statku

		this.rightPointX = this.x + Math.sin(Math.PI / 180 * (this.shipAngle + this.sternAngle)) * 0.6 * this.r; // sinA = x/r => x = sinA * r // * 0.6 dla zwezenia bokow statku
		this.rightPointY = this.y + Math.cos(Math.PI / 180 * (this.shipAngle + this.sternAngle)) * 0.6 * this.r; // cosA = y/r => y = cosA * r // * 0.6 dla zwezenia bokow statku

		this.upperPointX = this.x + Math.sin(Math.PI / 180 * (this.shipAngle + this.bowAngle)) * this.r; // sinA = x/r => x = sinA * r
		this.upperPointY = this.y + Math.cos(Math.PI / 180 * (this.shipAngle + this.bowAngle)) * this.r; // cosA = y/r => y = cosA * r

		
		//wyjscie statku poza granice canvas
		if (this.upperPointX < 0 && this.leftPointX < 0 && this.rightPointX < 0){
			this.x += window.innerWidth - Math.min(this.upperPointX, this.leftPointX, this.rightPointX); //przesuniecie statku o szerokosc okna i najbardziej wysuniety w lewo punkt x
		} else if (this.upperPointX > window.innerWidth && this.leftPointX > window.innerWidth && this.rightPointX > window.innerWidth) {
			this.x -= window.innerWidth + (Math.max(this.upperPointX, this.leftPointX, this.rightPointX) - window.innerWidth); //przesuniecie statku o szerokosc okna i najbardziej wysuniety w prawo punkt x
		}

		if(this.upperPointY < 0 && this.leftPointY < 0 && this.rightPointY < 0) {
			this.y += window.innerHeight - Math.min(this.upperPointY, this.leftPointY, this.rightPointY); //przesuniecie statku o wysokosc okna i najbardziej wysuniety w dol punkt y
		} else if (this.upperPointY > window.innerHeight && this.leftPointY > window.innerHeight && this.rightPointY > window.innerHeight) {
			this.y -= window.innerHeight + (Math.max(this.upperPointY, this.leftPointY, this.rightPointY) - window.innerHeight); //przesuniecie statku o wysokosc okna i najbardziej wysuniety w gore punkt y
		}

		// rysowanie sprite'a
		game.ctx.save();
		game.ctx.translate(this.x, this.y); // zabieg potrzebny do poprawnego obrotu statku wokol jego srodka // punktu (x, y)
		game.ctx.rotate(-this.shipAngle * Math.PI / 180); //obrot sprite'a (tak naprawde obrot game.ctx, cuda na kiju)
		game.ctx.translate(-this.x, -this.y); // zabieg potrzebny do poprawnego obrotu statku wokol jego srodka // punktu (x, y)

		// srodek sprite'a w punkcie this.x this.y (srodek trojkata)
		game.ctx.drawImage(this.sprite, 452, 10, 55, 45, this.x - this.width / 2, this.y - this.height / 2, this.width, this.height); // zielony
		game.ctx.drawImage(this.sprite, 736, 566, 59, 48, this.x - this.width / 2, this.y - this.height / 2, this.width, this.height);
		game.ctx.restore();
	}

	rand(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
}


	// background: url('imgs/spritesheet2.png') no-repeat -736px -566px;
	// width: 59px;
	// height: 48px;