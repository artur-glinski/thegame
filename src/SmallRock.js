class SmallRock {
	constructor(x, y) {
		this.id = ++game.smallRocksNumber; // zwiekszenie liczby malych kamieni od poczatku istnienia gry oraz przypisanie na jej podstawie id kamienia
		this.sprite = new Image();
		this.sprite.src = "./gfx/rock.png";

		this.r = this.rand(3, 9) * 0.004 * Math.min(window.innerWidth, window.innerHeight); // losowy promien kamienia
		this.speed = 0.00007 * this.rand(1, 5); // predkosc kamienia

		this.x = x // polozenie malego kamienia tam gdzie znajdowal sie ostatnio duzy kamien
		this.y = y // polozenie malego kamienia tam gdzie znajdowal sie ostatnio duzy kamien

		this.modX = ((this.rand(0,1) ? this.rand(-15, -5) : this.rand(5, 15))) * this.speed; // zmiana modX kamienia
		this.modY = ((this.rand(0,1) ? this.rand(-15, -5) : this.rand(5, 15))) * this.speed; // zmiana modY kamienia

		this.rockRotation = 0;
		this.changeRotation = this.rand(-4, 4);
	}

	bulletCollision() {
		for (var property in game.bulletsArray) {
			if (game.bulletsArray[property].x >= this.x - this.r && game.bulletsArray[property].x <= this.x + this.r &&
				game.bulletsArray[property].y >= this.y - this.r && game.bulletsArray[property].y <= this.y + this.r
				) {
				game.boomSound.play();
				delete game.smallRocksArray[this.id]; // usuwanie konkretnego kamienia w przypadku kolizji
				delete game.bulletsArray[property]; // usuwanie konkretnego pocisku w przypadku kolizji
				game.smallRocksNumber--;
				//this.points = Math.round(220 / this.r);
				game.score +=  Math.round(220 / this.r); // zliczanie punktow
				game.ctx.fillStyle = 'red';
				//game.ctx.fillText(this.points + ' pts', this.x, this.y); // wyswietlanie punktow w miejscu zniszczonego kamienia
				game.ctx.fillStyle = 'white';
			}				
		}
	}

	draw() {
		this.x += this.modX * window.innerWidth; // zmiana polozenia kamienia x
		this.y += this.modY * window.innerHeight; // zmiana polozenia kamienia y

		//wyjscie kamienia poza granice canvas
		if (this.x + this.r < 0) {
			this.x += window.innerWidth + (2 * this.r);
		} else if (this.x - this.r > window.innerWidth) {
			this.x -= window.innerWidth + (2 * this.r);
		}

		if (this.y + this.r < 0) {
			this.y += window.innerHeight + (2 * this.r);
		} else if (this.y - this.r > window.innerHeight) {
			this.y -= window.innerHeight + (2 * this.r);
		}

		this.rockRotation += this.changeRotation; //obrot kamienia w losowym kierunku i kacie
		// rysowanie sprite'a
		game.ctx.save();
		game.ctx.translate(this.x, this.y);
		game.ctx.rotate(this.rockRotation * Math.PI / 180);
		game.ctx.translate(-this.x, -this.y);
		game.ctx.drawImage(this.sprite, this.x - this.r, this.y - this.r, 2 * this.r, 2 * this.r);
		game.ctx.restore();
	}

	rand(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
}