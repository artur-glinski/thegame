game = {
	start: function() {
		game.canvas = document.createElement('canvas');
		game.ctx = game.canvas.getContext('2d');
		game.canvas.style.backgroundColor = 'black';
		game.layout();
		window.addEventListener('resize', game.layout);

		window.document.body.appendChild(game.canvas);

		game.goSound = new Howl({
			src: ['./sfx/go.mp3']
		});	
		game.themeSound = new Howl({
			src: ['./sfx/benny.mp3'],
			loop: true,
			html5: true
		});	
		game.shotSound = new Howl({
			src: ['./sfx/shot.mp3'],
			volume: 0.5
		});	
		game.boomSound = new Howl({
			src: ['./sfx/boom.mp3']
		});	
		game.overSound = new Howl({
			src: ['./sfx/over.mp3']
		});	
		game.successSound = new Howl({
			src: ['./sfx/success.mp3']
		});	

		game.goSound.play();
		game.themeSound.play();

		game.lives = 3; // zycia gracza
		game.ship = new Ship(); // tworzymy statek

		game.rocksArray = {}; // pseudo tablica na kamienie
		game.smallRocksArray = {}; // pseudo tablica na male kamienie
		game.bulletsArray = {}; // pseudo tablica na pociski

		game.rocksNumber = 0; // liczba istniejacych aktualnie kamieni, wartosc poczatkowa 0
		game.smallRocksNumber = 0; // liczba istniejacych aktualnie kamieni, wartosc poczatkowa 0
		game.bulletsNumber = 0; // liczba wszystkich wystrzelonych pociskow od poczatku istnienia gry, wartosc poczatkowa 0

		game.score = 0 // liczba punktow
		game.keysArray = {}; // pseudo tablica asocjacyjna na zapisywanie stanu przyciskow

		for (var i = 0; i < 15; i++) { // 15 kamieni na dobry poczatek
			game.rock = new Rock(); // stworzenie kamienia
			game.rocksArray[game.rock.id] = game.rock; // dodawanie kamienia do pseudo tablicy
		}

		window.addEventListener('keydown', game.keys); // przypisanie zdarzenia do nacisniecia przycisku
		window.addEventListener('keyup', game.keys); // przypisanie zdarzenia do puszczenia przycisku

		game.animation();
	},

	over: function() {
		window.removeEventListener('keydown', game.keys); // usuniecie zdarzenia do nacisniecia przycisku
		window.removeEventListener('keyup', game.keys); // usuniecie zdarzenia do puszczenia przycisku
		game.themeSound.stop();
		game.overSound.play();
		//alert('ale urwal, zdobyles: ' + game.score + ' punktow');
		
/*		$.ajax({
			url: './php/file.php',
			type: 'POST',
			data: {
				score: game.score,
			},
			success: function(msg) {
				console.log(msg);
			},
			error: function(msg) {
				console.log(msg);
			}
		});*/
	},

	animation: function(time) {
		requestAnimationFrame(game.animation);
		game.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight); // czyszczenie poprzedniej/kazdej klatki animacji

		game.ctx.fillText('player: ziomek', /*+ game.playerName,*/ 50, 50); // wyswietlanie uzytkownika
		game.ctx.fillText('score: ' + game.score, 50, 80); // wyswietlanie punktow
		game.ctx.fillText('lives: ' + game.lives, 50, 110); // wyswietlanie punktow

		game.ctx.fillStyle = 'white';

		if (game.ship) { // jesli istnieje statek to go rysuj i sprawdzaj kolizje
			game.ship.draw(); // rysowanie kamienia
			game.ship.rocksCollision(); // statek testuje kolizje z kamieniami w kazdej klatce animacji
		}
		
		if (game.rocksNumber > 0) { // jesli istnieja kamienie to je rysuj i sprawdzaj kolizje
			for (var property in game.rocksArray) {
				game.rocksArray[property].draw(); // rysowanie kamienia
				game.rocksArray[property].bulletCollision(); // kazdy kamien testuje kolizje z pociskami w kazdej klatce animacji
			}
		}
		
		if (game.smallRocksNumber > 0) { // jesli istnieja male kamienie to je rysuj i sprawdzaj kolizje
			for (var property in game.smallRocksArray) {
				game.smallRocksArray[property].draw(); // rysowanie kamienia
				game.smallRocksArray[property].bulletCollision(); // kazdy kamien testuje kolizje z pociskami w kazdej klatce animacji
			}
		}

		if (game.bulletsNumber > 0) { // jesli istnieja pociski rysuj je
			for (var property in game.bulletsArray) {
				game.bulletsArray[property].draw(); // rysowanie pocisku
			}
		}
	},

	keys: function(event) {
		if (event.keyCode === 32 || event.keyCode === 37 || event.keyCode === 38 || event.keyCode === 39 || event.keyCode === 40) {
			event.preventDefault();

			if (event.type === 'keydown' && game.keysArray['key_' + event.keyCode] !== true) {
				game.keysArray['key_' + event.keyCode] = true; // oznaczenie przycisku jako wcisniety

				if (event.keyCode === 37) {
					game.keysArray['key_39'] = false; // wcisniecie strzalki w lewo wylacza strzalke w prawo
				} else if (event.keyCode === 39) {
					game.keysArray['key_37'] = false; // wcisniecie strzalki w prawo wylacza strzalke w lewo
				} else if (event.keyCode === 38) {
					game.keysArray['key_32'] = false; // jakis bug, strzalka do przodu strzela pociskami?!
				} else if (game.keysArray['key_32'] === true) {
					game.shotSound.play();
					game.bullet = new Bullet(); // stworzenie pocisku
					game.bulletsArray[game.bullet.id] = game.bullet; // dodawanie pocisku do pseudo tablicy
				}
			} else if (event.type === 'keyup') {
				game.keysArray['key_' + event.keyCode] = false; // oznaczenie przycisku jako puszczony
			}
		}
	},

	layout: function() {
		game.canvas.width = window.innerWidth;
		game.canvas.height = window.innerHeight;
		game.ctx.fillStyle = 'white';
		game.ctx.strokeStyle = 'white';
		game.ctx.font='30px MATRIX';
	}
};


		// if (game.rocksNumber === 0 && game.smallRocksNumber === 0) {
		// 	console.log('zero');
		// 	game.themeSound.stop();
		// 	game.successSound.play();
		// }